import re
import os
import sys
import argparse
from OpenSSL import crypto, SSL


def key_util(key_file):
    key = crypto.PKey()
    if os.path.exists(key_file):
        print("loading " + key_file + " ...")
        f = open(key_file, "r")
        try:
            key = crypto.load_privatekey(crypto.FILETYPE_PEM, f.read())
        except:
            print("error: " + key_file + " could not be loaded. please check key file")
            sys.exit(1)
        else:
            return key
        finally:
            f.close()
    else:
        print("generating new key ...")
        TYPE_RSA = crypto.TYPE_RSA
        key.generate_key(TYPE_RSA, 4096)
        f = open(key_file, "wb")
        f.write(crypto.dump_privatekey(crypto.FILETYPE_PEM, key))
        f.close()
        return key


# subject data to use for TLS functions
def cert_subject(cert, domain_name, issuer):
    cert.get_subject().C = "US"
    cert.get_subject().ST = "California"
    cert.get_subject().L = "Palo Alto"
    cert.get_subject().O = "Stanford Medicine"
    cert.get_subject().OU = "Anesthesia IT"
    cert.get_subject().CN = domain_name
    if issuer == "internal":
        cert.set_serial_number(1000)
        cert.gmtime_adj_notBefore(0)
        cert.gmtime_adj_notAfter(315360000)
        cert.set_issuer(cert.get_subject())
    elif issuer != "trusted":
        print("invalid issuer value")


# generate signing request
def signing_req(key_file, csr_file, domain_name):
    req = crypto.X509Req()
    cert_subject(req, domain_name, "trusted")

    key = key_util(key_file)
    req.set_pubkey(key)
    req.sign(key, "sha256")

    f = open(csr_file, "wb")
    f.write(crypto.dump_certificate_request(crypto.FILETYPE_PEM, req))
    f.close()

    with open(csr_file, 'r') as f:
        print(f.read())
    f.close()

    print("created signing request for " + url)
    print("CSR: " + csr_file)

# generate self-signing certificate
def create_self_signed(key_file, cert_file, domain_name):
    cert = crypto.X509()
    cert_subject(cert, domain_name, "internal")

    key = key_util(key_file)
    cert.set_pubkey(key)
    cert.sign(key, "sha256")

    f = open(cert_file, "wb")
    f.write(crypto.dump_certificate(crypto.FILETYPE_PEM, cert))
    f.close()

    with open(cert_file, 'r') as f:
        print(f.read())
    f.close()

    print("created self-signing certificate for " + url)
    print("CERT: " + cert_file)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Utility for Managing Certificates')
    parser.add_argument('--domainname', dest='domain_name', action='store', required=True, help='Fully Qualified Domain Name Needed')
    parser.add_argument('--certpath', dest='cert_path', action='store', default='/etc/ssl/certs', help='Path to Certificates')
    parser.add_argument('--keypath', dest='key_path', action='store', default='/etc/ssl/private', help='Path to Private Keys')
    parser.add_argument('--suffix', dest='suffix_type', action='store', choices=['legacy', 'pem'], default='legacy', help='Suffix to use for Private Key')
    uploadTypeGroup = parser.add_mutually_exclusive_group(required=True)
    uploadTypeGroup.add_argument('--newcsr', dest='new_csr', action='store_true', help='New Signing Request')
    uploadTypeGroup.add_argument('--self-signed-cert', dest='self_signed_cert', action='store_true', help='New Self Signing Cert')
    args = parser.parse_args()

pattern = "[a-z0-9]+.stanford.edu\Z"
try:
    m = re.search(pattern, args.domain_name)
    if m == None:
        raise Exception
except Exception:
    print('domain name "' + args.domain_name + '" is not valid. exiting ...')
    sys.exit(1)

formatted_hostname = args.domain_name.replace(".", "_")
if args.new_csr == True:
    csr_file = args.cert_path + "/" +  formatted_hostname + ".csr"
    if args.suffix_type == "legacy":
        key_file = args.key_path + "/" + formatted_hostname + ".key"
    elif args.suffix_type == "pem":
        key_file = args.key_path + "/" + formatted_hostname + "_key.pem"

    url = "https://" + args.domain_name

    signing_req(key_file, csr_file, args.domain_name)

if args.self_signed_cert == True:
    formatted_hostname = formatted_hostname + "-self"
    key_file = args.key_path + "/" + formatted_hostname + ".key"
    cert_file = args.cert_path + "/" + formatted_hostname + ".cer"
    url = "https://" + args.domain_name

    create_self_signed(key_file, cert_file, args.domain_name)
